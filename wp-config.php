<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'danmad__tmp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'cMj? iGW)vj/b{9r$vY@h w_t1Zo3_t<q!~Xei>:sqLl/?cTuH[EFX/BM3;08$Lg' );
define( 'SECURE_AUTH_KEY',  ')EDgOMweW@LB~<8;wvsy9HTi?r(9_A}pMo}lD=;>Sgu4L:EoGKG6N-eSmONZ|5SE' );
define( 'LOGGED_IN_KEY',    '(<)jjFLeLPeBkElRWAH]B:Cuu@+2Uu+p5<7x.,LFnh|7hg#f56~a:jX^jo$7n+EJ' );
define( 'NONCE_KEY',        ' 0FPyQBwjq3sU65,}pk&0sm4}S=/|>Mjq;#OPcA>0:7DMnl3VR#YCiR^7W1o9In2' );
define( 'AUTH_SALT',        'h7Yai.|),:ff6};nSKtaKBwBG.!<{w(_y_|y#&_)qmy::o`?7gQh.zHXT1pKPeb3' );
define( 'SECURE_AUTH_SALT', 's=7Zly#f$UmKBpRfy/5%y=9y.Bvg3^I=)Pm3t2+BB4aW>9S~/:6HN2jG[D-n1mF|' );
define( 'LOGGED_IN_SALT',   'T8p7uw}VAa^>;-@FnqDx}mmVhG<yp-RT<(RqW]@k(iMV,}!z Zpu/nkO<Vc,f%<&' );
define( 'NONCE_SALT',       'y)tJYIKZxe#on#gzjE4u,=wP&P:y<z~TT-a:[Ut,g7f<ct ?8BI_=/4L:9qvBiY%' );

/**#@-*/


define('FS_METHOD','direct');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
