<?php
/*
Plugin Name: appsaz mailpoet rest API
Description: add rest API support to mail poet
Version: 1.0.0
Author: Amir Sasani
Author URI: http://appsaz.ir
License: GPL2
*/


class AppsazMailpoetRestAPI
{
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'my_register_route']);
    }

    function my_register_route()
    {
        register_rest_route(
            'as_mailpoet',
            'subscribe',
            array(
                'methods' => 'GET',
                'callback' => [$this, 'as_mailpoet_subscribe_user'],
            )
        );
    }

    function as_mailpoet_subscribe_user(WP_REST_Request $request)
    {
        $data = $request->get_params();
        $email = $data['email'];

        $data_to_return = [];
        $data_to_return['success'] = false;

        if (!empty($email)) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

                if (class_exists(\MailPoet\API\API::class)) {
                    try {
                        $mailpoet_api = \MailPoet\API\API::MP('v1');
                        $mailpoet_api->addSubscriber(['email' => $email]);

                        $data_to_return['success'] = true;
                        $data_to_return['data'] = 'user successfully subscribed!';
                    } catch (Exception $e) {
                        $data_to_return['success'] = false;

                        $errorMsg = '';
                        switch ($e->getCode()) {
                            case 11:
                                $errorMsg = 'Missing email address';
                                break;

                            case 12:
                                $errorMsg = 'Trying to create a subscriber that already exists';
                                break;

                            case 13:
                                $errorMsg = 'The subscriber couldn’t be created in the database';
                                break;

                            case 17:
                                $errorMsg = 'Welcome email failed to send';
                                break;

                            default:
                                $errorMsg = 'There is a problem in this process';
                                break;
                        }

                        $data_to_return['data'] = $errorMsg;
                    }
                } else {
                    $data_to_return['data'] = 'There is an error in the process: Mailpoet is not activated';
                }


            } else {
                $data_to_return['data'] = 'email is not valid!';
            }
        } else {
            $data_to_return['data'] = 'email is required!';
        }

        return new WP_REST_Response($data_to_return);
    }

}

new AppsazMailpoetRestAPI();
