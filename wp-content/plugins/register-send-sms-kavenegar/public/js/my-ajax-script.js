jQuery(function(){
    jQuery('.error').hide();
    var ajaxurl="<?php echo 'http://localhost/elmart/wp-admin/plugin/register-send-sms-kavenegar/admin-ajax.php';?>";
    jQuery("#submitcode").click(function(){
        jQuery('.error').hide();
        var user_mobile = jQuery("input#user_mobile").val();
        if (user_mobile == "") {
            jQuery("label#user_mobile_error").show();
            jQuery("input#user_mobile").focus();
            return false;
        }

        else {
            var formdata = jQuery("#code_mobile  :input").serialize();
            formdata += "&action=code_send&param=code_test";
            jQuery.ajax({
                url: my_ajax_object.ajax_url,
                data: formdata,
                type: "POST",
                beforeSend:function() {
                    jQuery(".loading").show();
                },
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    if (data.status == 1) {
                        jQuery("#code_mobile").hide();
                        jQuery("#code_user").show();
                        jQuery("#coderesive").val(data.code);

                    } else {
                        alert("متاسفانه ارسال کد با خطا مواجه شد");
                    }

                },
                complete: function() {
                    jQuery(".loading").hide();
                }

            });
        }
    });
});