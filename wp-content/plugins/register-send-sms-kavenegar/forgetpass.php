<?php
/** Template Name: customForgetpass 
 */


get_header()
?>
<div id="lostPassword">
		<div id="message"></div>
		<form id="lostPasswordForm" method="post">
			< ?php
				// this prevent automated script for unwanted spam
				if ( function_exists( 'wp_nonce_field' ) ) 
					wp_nonce_field( 'rs_user_lost_password_action', 'rs_user_lost_password_nonce' );
			?>
 
			<p>
				<label for="user_login">< ?php _e('Username or E-mail:') ?> <br />
					<input type="text" name="user_login" id="user_login" class="input" value="<?php echo esc_attr($user_login); ?>" size="20" />
				</label>
			</p>
			< ?php
			/**
			 * Fires inside the lostpassword <form> tags, before the hidden fields.
			 *
			 * @since 2.1.0
			 */
			do_action( 'lostpassword_form' ); ?>
			<p class="submit">
				<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php esc_attr_e('Get New Password'); ?>" />
				<img src="<?php echo get_stylesheet_directory_uri(); ?>" />/images/ajax-loader.gif" id="preloader" alt="Preloader" />
			</p>
		</form>
	</div>
	<div id="resetPassword">
	<div id="message"></div>
	<!--this check on the link key and user login/username-->
	< ?php
		$errors = new WP_Error();
		$user = check_password_reset_key($_GET['key'], $_GET['login']);
 
		if ( is_wp_error( $user ) ) {
			if ( $user->get_error_code() === 'expired_key' )
				$errors->add( 'expiredkey', __( 'Sorry, that key has expired. Please try again.' ) );
			else
				$errors->add( 'invalidkey', __( 'Sorry, that key does not appear to be valid.' ) );
		}
 
		// display error message
		if ( $errors->get_error_code() )
			echo $errors->get_error_message( $errors->get_error_code() );
		?>
 
		<form id="resetPasswordForm" method="post" autocomplete="off">
			< ?php
				// this prevent automated script for unwanted spam
				if ( function_exists( 'wp_nonce_field' ) ) 
					wp_nonce_field( 'rs_user_reset_password_action', 'rs_user_reset_password_nonce' );
			?>
			
			<input type="hidden" name="user_key" id="user_key" value="<?php echo esc_attr( $_GET['key'] ); ?>" autocomplete="off" />
			<input type="hidden" name="user_login" id="user_login" value="<?php echo esc_attr( $_GET['login'] ); ?>" autocomplete="off" />
 
			<p>
				<label for="pass1">< ?php _e('New password') ?><br />
				<input type="password" name="pass1" id="pass1" class="input" size="20" value="" autocomplete="off" /></label>
			</p>
			<p>
				<label for="pass2">< ?php _e('Confirm new password') ?><br />
				<input type="password" name="pass2" id="pass2" class="input" size="20" value="" autocomplete="off" /></label>
			</p>
 
			<p class="description indicator-hint">< ?php _e('Hint: The password should be at least seven characters long. To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ & ).'); ?></p>
 
			<br class="clear" />
 
			< ?php
			/**
			 * Fires following the 'Strength indicator' meter in the user password reset form.
			 *
			 * @since 3.9.0
			 *
			 * @param WP_User $user User object of the user whose password is being reset.
			 */
			do_action( 'resetpass_form', $user );
			?>
			<p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php esc_attr_e('Reset Password'); ?>" />
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif" id="preloader" alt="Preloader" />
			</p>
		</form>
	</div>
<?php get_footer() ?>