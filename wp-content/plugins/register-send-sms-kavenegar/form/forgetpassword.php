<div class=" register-plugin">
    <div class="col-md-12">
        <p class="error" id="error-forget"></p>
        <div class="loading" style="display:none; width: 47%;height: 138px; margin: 0 26%;">
            <div class="loading-wrap">
                <div class="triangle1"></div>
                <div class="triangle2"></div>
                <div class="triangle3"></div>
            </div>
        </div>
        <div id="code_mobile">
            <div class="form-group">
                <div id="forget_password">
                    <p class="status"></p>
                    <label for="username">نام کاربری (شماره موبایل) </label>
                    <input type="text" name="username" id="username" placeholder="نام کاربری" class="ajax-search_text form-control">
                    <input type="submit" id="forget_pass" value="ارسال"/>
                </div>
                <div id="Confirmation_code" style="display:none">
                    <p class="status"></p>
                    <label for="code_user">کد دریافتی</label>
                    <input type="text" name="pagelist" placeholder="کد فعال سازی" class="ajax-code_text form-control">
                    <input type="hidden" id="user_name_hidden">
                    <input type="submit" id="forget_code" value="تایید کد دریافتی"/>
                </div>
                <div class="heycode-ajax-result">
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function myRandom(start,end){
        randomNumber = start + Math.floor(Math.random() * (end-start));
        return randomNumber;
    }
    jQuery(document).ready(function($){
        //when keyup on textbox
        $("#forget_pass").click(function(){
            jQuery("#error-forget").hide();
            var keyValue = $('.ajax-search_text').val();
            if(keyValue){
                $(".status").text("");
                $.ajax({
                    type : "POST",
                    url  : "https://salembekhar.com/wp-admin/admin-ajax.php",
                    data : 'action=get_search_list_via_ajax&text='+ keyValue ,
                    beforeSend: function(){
                        jQuery('.loading').show();
                    },
                    success: function(response) {
                        $('loading').css('display','none');
                        if(response==0){
                            jQuery("#error-forget").html("نام کاربری وجود ندارد");
                            jQuery("#error-forget").show();
                        }
                        if(response==1){
                            $("#user_name_hidden").val(keyValue);
                            window.codeuser=myRandom(11111,99999);
                            jQuery.ajax({
                                url: 'https://api.kavenegar.com/v1/506F317A50637A4D715637614B4E79345A6E69724C3630474E6134463137416B373643337735436C426F773D/verify/lookup.json?receptor='+keyValue+'&token='+window.codeuser+'&template=petroraad',                      
                                type: "get",
                                success: function (response) {
                                    if(response.return.status==200){
                                        jQuery("#forget_password").hide();
                                        jQuery("#Confirmation_code").show();
                                    }
                                    else {
                                        jQuery("#error-complete").html("متاسفانه شبکه با مشکل مواجه شد چند دقیقه بعد باز امتحان کنید");

                                    }
                                },
                            });
                        }
                    },
                    complete: function() {
                        jQuery(".loading").hide();
                    }
                });
            }else{
                jQuery("#error-forget").html("لطفا نام کاربری خود را وارد کنید");     
                 jQuery("#error-forget").show();
            }
        });
        $("#forget_code").click(function(){
            jQuery("#error-forget").hide();
            var code_user = $('.ajax-code_text').val();
            var user_name= $("#user_name_hidden").val();
            if(code_user){
                $.ajax({
                    type : "POST",
                    url  : "https://salembekhar.com/wp-admin/admin-ajax.php",
                    data : 'action=get_code_list_via_ajax&text='+code_user+'&user_name='+user_name+'&code_get='+window.codeuser ,
                    beforeSend: function(){
                          jQuery(".loading").show();
                    },
                    success: function(response) {
                        //past ajax result on result div.
                        if(response==0){
                            jQuery("#error-forget").html("کد دریافتی را به درستی وارد کنید");    
                            jQuery("#error-forget").show();
                        }
                        else{
                            jQuery.ajax({
                                url: 'https://api.kavenegar.com/v1/506F317A50637A4D715637614B4E79345A6E69724C3630474E6134463137416B373643337735436C426F773D/verify/lookup.json?receptor='+user_name+'&token='+JSON.parse(response)+'&template=passwordnew',                                        type: "get",
                                success: function (response) {
                                    if(response.return.status==200){
                                        window.location.href = "<?php echo user_admin_url()?>";
                                    }
                                    else {
                                        jQuery("#error-complete").html("متاسفانه شبکه با مشکل مواجه شد چند دقیقه بعد باز امتحان کنید");
                                    }
                                },
                            });
                        }
                    },
                    complete: function() {
                         jQuery(".loading").hide();
                    }
                });
            }else{
                $(".heycode-ajax-result").html("");
            }
        });
    });
</script>