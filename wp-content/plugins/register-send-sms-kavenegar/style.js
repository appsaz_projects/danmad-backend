jQuery(document).ready( function() {
    jQuery(function () {
        jQuery('.error').hide();
        var ajaxurl = "<?php echo admin_url('admin-ajax.php')?>";
        jQuery("#submitcode").click(function () {
            jQuery('.error').hide();
            var user_mobile = jQuery("input#user_mobile").val();
            if (user_mobile == "") {
                jQuery("label#user_mobile_error").show();
                jQuery("input#user_mobile").focus();
                return false;
            } else {
                var formdata = jQuery("#code_mobile  :input").serialize();
                formdata += "&action=code_send&param=code_test";
                jQuery.ajax({
                    url: ajaxurl,
                    data: formdata,
                    type: "POST",
                    success: function (response) {
                        var data = jQuery.parseJSON(response);
                        if (data.status == 1) {
                            jQuery("#code_mobile").hide();
                            jQuery("#code_user").show();
                            jQuery("#coderesive").val(data.code);

                        } else {
                            alert("متاسفانه ارسال کد با خطا مواجه شد");
                        }

                    }

                });
            }
        });
    });
    jQuery(function () {
        jQuery('.error').hide();
        var ajaxurl = "<?php echo admin_url('admin-ajax.php')?>";
        jQuery("#submituser_code").click(function () {
            jQuery('.error').hide();
            var user_code = jQuery("input#user_code").val();
            if (user_code == "") {
                jQuery("label#user_code_error").show();
                jQuery("input#user_code").focus();
                return false;
            } else {
                var formdata = jQuery("#code_user  :input").serialize();
                formdata += "&action=code_get&param=code_testt";
                jQuery.ajax({
                    url: ajaxurl,
                    data: formdata,
                    type: "POST",
                    success: function (response) {
                        var data = jQuery.parseJSON(response);
                        if (data.status == 1) {
                            jQuery("#code_user").hide();
                            jQuery("#form_register").show();

                        } else {
                            alert("متاسفانه ارسال کد با خطا مواجه شد");
                        }

                    }

                });
            }
        });
    });
    jQuery(function () {
        jQuery('.error').hide();
        var ajaxurl = "<?php echo admin_url('admin-ajax.php')?>";
        jQuery("#submitbtn").click(function () {
            jQuery('.error').hide();
            var user_login = jQuery("input#first_name").val();
            if (user_login == "") {
                jQuery("label#user_login_error").show();
                jQuery("input#user_login").focus();
                return false;
            }
            var user_password = jQuery("input#user_password").val();
            if (user_password == "") {
                jQuery("label#user_password_error").show();
                jQuery("input#user_password").focus();
                return false;
            } else {
                var formdata = jQuery("#wp_login_form  :input").serialize();
                formdata += "&action=custom_login&param=login_test";
                jQuery.ajax({
                    url: ajaxurl,
                    data: formdata,
                    type: "POST",
                    success: function (response) {
                        var data = jQuery.parseJSON(response);
                        if (data.status == 1) {
                            window.location = "<?php site_url()?>";
                        } else {
                            alert("invalid user/password");
                        }

                    }

                });
            }
        });
    });

    jQuery(function () {
        var ajaxurl = "<?php echo admin_url('admin-ajax.php')?>";
        jQuery('.error').hide();
        jQuery("#register-btn").on('click', function () {
            jQuery('.error').hide();
            var first_name = jQuery("input#first_name").val();
            if (first_name == "") {
                jQuery("label#first_name_error").show();
                jQuery("input#first_name").focus();
                return false;
            }
            var last_name = jQuery("input#last_name").val();
            if (last_name == "") {
                jQuery("label#last_name_error").show();
                jQuery("input#last_name").focus();
                return false;
            }
            var email = jQuery("input#email").val();
            if (email == "") {
                jQuery("label#email_error").show();
                jQuery("input#email").focus();
                return false;
            }
            var mobile = jQuery("input#mobile").val();
            if (mobile == "") {
                jQuery("label#mobilee_error").show();
                jQuery("input#mobile").focus();
                return false;
            }
            var pwd1 = jQuery("input#pwd1").val();
            if (pwd1 == "") {
                jQuery("label#pwd1_error").show();
                jQuery("input#pwd1").focus();
                return false;
            }
            var pwd2 = jQuery("input#pwd2").val();
            if (pwd2 == "") {
                jQuery("label#pwd2_error").show();
                jQuery("input#pwd2").focus();
                return false;
            } else {
                var formdata = jQuery("#form_register :input").serialize();
                formdata += "&action=custom_register&param=register_test";
                jQuery.ajax({
                    url: ajaxurl,
                    type: "POST",
                    data: formdata,
                    success: function (response) {
                        var data = jQuery.parseJSON(response);
                        alert(data.status);

                    }

                })
            }
        });
    });
});