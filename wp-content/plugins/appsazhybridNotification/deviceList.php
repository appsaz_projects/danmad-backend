<?php
if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Device_List extends WP_List_Table
{
    public function __construct()
    {
        parent::__construct([
            'singular' => __('Device', 'sp'), //singular name of the listed records
            'plural' => __('Devices', 'sp'), //plural name of the listed records
            'ajax' => false //should this table support ajax?
        ]);
    }

    public static function get_devices($per_page, $page_number = 1)
    {
        global $wpdb;
        $sql = "SELECT * FROM appsazhybrid_devices";
        if (!empty($_REQUEST['orderby'])) {
            $sql .= ' ORDER BY ' . esc_sql($_REQUEST['orderby']);
            $sql .= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : ' ASC';
        } else {
            $sql .= ' ORDER BY userId DESC';
        }
        $sql .= " LIMIT $per_page";
        $sql .= ' OFFSET ' . ($page_number - 1) * $per_page;
        $results = $wpdb->get_results($sql, 'ARRAY_A');
        for ($i = 0; $i < count($results); $i++) {
            $results[$i]['id'] = $i + 1;
            $sql = "SELECT COUNT(*) FROM appsazhybrid_user_message WHERE userId='" . $results[$i]['userId'] . "'";
            $results[$i]['sentMessages'] = $wpdb->get_var($sql);
            $sql = "SELECT COUNT(*) FROM appsazhybrid_user_message WHERE delivery=1 AND userId='" . $results[$i]['userId'] . "'";
            $results[$i]['receivedMessages'] = $wpdb->get_var($sql);
            $sql = "SELECT COUNT(*) FROM appsazhybrid_user_message WHERE seen=1 AND userId='" . $results[$i]['userId'] . "'";
            $results[$i]['seenMessages'] = $wpdb->get_var($sql);
        }
        return $results;
    }

    //TODo
    /*	public static function delete_device($id)
        {
            global $wpdb;
            $wpdb->delete(
                "appsaz_devices",
                ['ID' => $id],
                ['%d']
            );
        }*/

    public static function record_count()
    {
        global $wpdb;
        $sql = "SELECT COUNT(*) FROM appsazhybrid_devices";
        return $wpdb->get_var($sql);
    }

    function get_columns()
    {
        $columns = array(
            'id' => __('No.'),
            'category' => __('Category'),
            'deviceType' => __('Device type'),
            'sentMessages' => __('Messages sent'),
            'receivedMessages' => __('Messages delivered'),
            'seenMessages' => __('Messages seen'),
        );
        return $columns;
    }

    function column_default($item, $column_name)
    {
        switch ($column_name) {
            case 'id':
            case 'category':
            case 'deviceType':
            case 'receivedMessages':
            case 'sentMessages':
            case 'seenMessages':
                return $item[$column_name];
            default:
                return print_r($item, true); //Show the whole array for troubleshooting purposes
        }
    }

    function get_sortable_columns()
    {
        $columns = array(
            'category' => array('category', false),
            'deviceType' => array('deviceType', false)
        );
        return $columns;
    }

    function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $devicePerPage = 30;
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = self::get_devices($devicePerPage, $this->get_pagenum());
        $this->set_pagination_args(array(
            'total_items' => self::record_count(),                  //WE have to calculate the total number of items
            'per_page' => $devicePerPage
        ));
    }
}

function appsazMessageDevicesPage()
{
    ?>
    <div class="wrap">
        <h2><?php _e('Members list') ?></h2>
        <?php
        $deviceList = new Device_List();
        $deviceList->prepare_items();
        $deviceList->display();
        ?>
    </div>
    <?php
}
