<?php

add_action('admin_menu', 'appsazNotificationAdminMenu');
function appsazNotificationAdminMenu()
{
    //add_menu_page('ارسال پیام', 'ارسال پیام', 'active_plugins', 'appsazMessage', 'appsazInboxAdminPage', 'dashicons-cart', 6);
    //add_submenu_page('appsaz','ارسال پیام', 'ارسال پیام', 'activate_plugins',  'admin.php?page=appsazMessage');

    add_menu_page(__('Push notification'), __('Push notification'), 'activate_plugins', 'appsazMessage', 'appsazInboxAdminPage');
    add_submenu_page('appsazMessage', __('Messages list'), __('Messages list'), 'manage_options', 'appsazMessageReports', 'appsazMessageReportsPage');
    add_submenu_page('appsazMessage', __('Members'), __('Members'), 'manage_options', 'appsazMessageDevices', 'appsazMessageDevicesPage');
    add_submenu_page(null, __('Message details'), __('Message details'), 'manage_options', 'appsazMessageDetail', 'appsazMessageDetailPage');
}

add_action('all_admin_notices', 'appsaz_notification_navtab');
function appsaz_notification_navtab()
{
    $page = [];
    if (isset($_REQUEST['page'])) {
        $page = checkNotificationPage($_REQUEST['page']);
    }
    if (count($page) == 2 && strcmp($page[0], 'appnotification') == 0) {
        ?>
        <h2 class="nav-tab-wrapper" style="border-bottom: 2px solid gray">
            <a class="nav-tab <?php echo $page[1] == 1 ? 'nav-tab-active' : '' ?>"
               href="<?php echo admin_url('admin.php?page=appsazMessage'); ?>"><?php _e('Send message') ?></a>
            <a class="nav-tab <?php echo $page[1] == 2 ? 'nav-tab-active' : '' ?>"
               href="<?php echo admin_url('admin.php?page=appsazMessageReports'); ?>"><?php _e('Messages list') ?> </a>
            <a class="nav-tab <?php echo $page[1] == 3 ? 'nav-tab-active' : '' ?>"
               href="<?php echo admin_url('admin.php?page=appsazMessageDevices'); ?>"><?php _e('Members') ?> </a>
        </h2>
        <?php
    }

}

function checkNotificationPage($page)
{
    $menu = [];
    if (strcmp($page, "appsazMessage") == 0) {
        $menu[] = "appnotification";
        $menu[] = 1;
    } else if (strcmp($page, "appsazMessageReports") == 0) {
        $menu[] = "appnotification";
        $menu[] = 2;
    } else if (strcmp($page, "appsazMessageDetail") == 0) {
        $menu[] = "appnotification";
        $menu[] = 2;
    } else if (strcmp($page, "appsazMessageDevices") == 0) {
        $menu[] = "appnotification";
        $menu[] = 3;
    }

    return $menu;

}


