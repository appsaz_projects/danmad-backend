<p><?php printf("Følgeseddel %s", $order->get_id()) ?></p>

<?php
$customer_name = '';

if (!empty($order->get_billing_first_name())) {
    $customer_name .= $order->get_billing_first_name();
}

if (!empty($order->get_billing_last_name())) {
    $customer_name .= ' ' . $order->get_billing_last_name();
}

if (!empty($order->get_billing_company())) {
    $customer_name .= ' ';
    $customer_name .= '(' . $order->get_billing_company() . ')';
}
?>

<?php
$order_id = $order->get_id();
$order_id_hash = get_post_meta($order_id, 'order_hash', true);
if(empty($order_id_hash)){
    $order_id_hash = md5($order_id);
}
$download_pdf_url = get_rest_url(null, "invoices/download_pdf");
$download_pdf_url .= "?id=${order_id_hash}";
?>

<p><?= sprintf("Kære %s", $customer_name) ?></p>

<p style="margin-bottom: 0;">Tak for din bestille/ordre</p>
<p style="margin-top: 0;">Vi har modtaget din ordre, og den behendles nu.</p>

<br>

<p>Klik på linket herunder for at download følgeseddel</p>
<a href="<?= $download_pdf_url; ?>"
   style="padding: 10px; border-radius: 5px; color: #ffffff; background-color: #00944b;text-decoration: none;">
        Hent din følgeseddel
</a>


<p>Med venlig hilsen</p>
<p>DANMAD</p>
