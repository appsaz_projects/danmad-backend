<p><?php printf("Faktura %s", $order->get_id()) ?></p>

<?php
$customer_name = '';

if (!empty($order->get_billing_first_name())) {
    $customer_name .= $order->get_billing_first_name();
}

if (!empty($order->get_billing_last_name())) {
    $customer_name .= ' ' . $order->get_billing_last_name();
}

if (!empty($order->get_billing_company())) {
    $customer_name .= ' ';
    $customer_name .= '(' . $order->get_billing_company() . ')';
}

?>

<?php
$order_id = $order->get_id();
$order_id_hash = get_post_meta($order_id, 'order_hash', true);
if (empty($order_id_hash)) {
    $order_id_hash = md5($order_id);
    update_post_meta($order_id, 'order_hash', $order_id_hash);
}
$download_pdf_url = get_rest_url(null, "invoices/download_pdf");
$download_pdf_url .= "?id=${order_id_hash}";
?>

<p><?= sprintf("Kære %s", $customer_name) ?></p>

<p style="margin-bottom: 0;">Tusind tak fordi, du har valgt at være kunde hos DANMAD.</p>
<p style="margin-top: 0;"><?php printf("Her er din faktura %d på %s. ", $order->get_id(), wc_price($order->get_total())) ?></p>


<p>Klik på linket herunder for at download fakturaen.</p>
<a href="<?= $download_pdf_url; ?>"
   style="padding: 10px; border-radius: 5px; color: #ffffff; background-color: #00944b;text-decoration: none;">
    Hent din faktura
</a>

<p>Med venlig hilsen</p>
<p>DANMAD</p>
