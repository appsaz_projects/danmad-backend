<?php
/*
Plugin Name: appsaz duplicate order
Description: duplicates an order
Version: 1.0.0
Author: Amir Sasani
Author URI: http://appsaz.ir
License: GPL2
*/


require "class-wc-admin-duplicate-order.php";
require dirname(__DIR__) . "/mstoreapp-mobile-app-with-sms-master/public/class-mstoreapp-mobile-app-public.php";

class appsazDuplicateOrder
{
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'init_rest']);
    }

    function init_rest()
    {

        register_rest_route(
            'as_appsaz',
            'get-order-items',
            [
                'methods' => WP_REST_Server::ALLMETHODS,
                'callback' => [$this, 'get_order_items']
            ]
        );

        register_rest_route(
            'as_appsaz',
            'duplicate-order',
            [
                'methods' => WP_REST_Server::ALLMETHODS,
                'callback' => [$this, 'duplicate_order']
            ]
        );

        register_rest_route(
            'as_appsaz',
            'add-products-to-cart',
            [
                'methods' => WP_REST_Server::ALLMETHODS,
                'callback' => [$this, 'add_products_to_cart']
            ]
        );

    }

    function get_order_items(WP_REST_Request $request)
    {
        $data = $request->get_params();
        $order_id = $data['order_id'];

        $data_to_return = [];

        if (!empty($order_id)) {

            try {
                $order = new WC_Order($order_id);

                $order_line_items = $order->get_items();
                $items = [];
                $is_order_items_changed = false;
                $coupons = $order->get_coupons();

                foreach ($order_line_items as $item) {

                    $itemName = $item['name'];
                    $qty = $item['qty'];
                    $lineTotalOld = $item['line_total'];
                    $lineTotalNew = null;
                    $lineTax = $item['line_tax'];
                    $productID = $item['product_id'];
                    $variationId = $item['variation_id'];
                    $variationName = null;

                    if ($variationId == 0) {
                        $product = wc_get_product($productID);
                    } else {
                        $product = wc_get_product($variationId);
                        $variationName = $product->get_attributes();
                    }

                    if ($product) {
                        $lineTotalNew = $product->get_price();
                    }

                    if (($lineTotalOld / $qty) != intval($lineTotalNew)) {
                        $is_order_items_changed = true;
                    }

                    if (!$product->is_in_stock()) {
                        $is_order_items_changed = true;
                    }

                    $stock_amount = -1;
                    if ($product->get_manage_stock()) {
                        $stock_amount = $product->get_stock_quantity();
                    }

                    $items[] = [
                        'name' => $itemName,
                        'qty' => $qty,
                        'line_total_old' => $lineTotalOld / $qty,
                        'line_total_new' => floatval($lineTotalNew),
                        'item_price' => $lineTotalOld / $qty,
                        'line_tax' => $lineTax,
                        'product_id' => $productID,
                        'variationId' => $variationId,
                        'variationName' => $variationName,
                        'stock_amount' => $stock_amount,
                    ];
                }

                $data_to_return['success'] = true;
                $data_to_return['is_order_items_changed'] = $is_order_items_changed;
                $data_to_return['items'] = $items;

            } catch (Exception $e) {
                $data_to_return['success'] = false;
                $data_to_return['msg'] = $e->getMessage();
            }
        } else {
            $data_to_return['success'] = false;
            $data_to_return['msg'] = 'Order id is required!';
        }
        return new WP_REST_Response($data_to_return);
    }

    function duplicate_order(WP_REST_Request $request)
    {
        $data = $request->get_params();
        $order_id = $data['order_id'];

        $data_to_return = [];

        if (!empty($order_id)) {

            try {
                $new_order = new WC_Order($order_id);
                $duplicate_class = new WC_Admin_Duplicate_Order();

                $duplicate_result = $duplicate_class->duplicate_order($new_order);
                $data_to_return = [
                    'success' => true,
                    'result' => 'success',
                    'new_order_id' => $duplicate_result
                ];
            } catch (Exception $e) {
                $data_to_return = [
                    'success' => false,
                    'result' => 'error',
                    'msg' => $e->getMessage()
                ];
            }
        } else {
            $data_to_return = [
                'success' => false,
                'result' => 'error',
                'msg' => 'The order id is required!'
            ];
        }
        return new WP_REST_Response($data_to_return);
    }

    function add_products_to_cart(WP_REST_Request $request)
    {
//        $mstore_class = new Mstoreapp_Mobile_App_Public("mstoreapp-mobile-app", "1.0.0");

        $data = $request->get_params();
        $user_id = $data['user_id'];
        $productsList = $data['productsList'];

        $data_to_return = [];
        $data_to_return['success'] = false;


        $user = get_user_by('ID', $user_id);
        if ($user) {

            if (!empty($productsList)) {
                $data_to_return['success'] = true;

                if (is_string($productsList)) {
                    $productsList = json_decode($productsList);
                }

                if (!is_array($productsList)) {
                    $productsList = [$productsList];
                }

                try {

                    WC()->frontend_includes();
                    WC()->session = new WC_Session_Handler();
                    WC()->session->init();
                    WC()->customer = new WC_Customer($user->ID, true);
                    WC()->cart = new WC_Cart();

                    WC()->cart->empty_cart();
//                    $mstore_class->emptyCart();

                    foreach ($productsList as $product_id) {
//                        $product_id = intval($product_object['product_id']);
//                        $qty = intval($product_object['qty']);
//                        $variationId = $product_object['variationId'];
//                        $variationArray = $product_object['variationName'];

//                        if (empty($variationArray)) {
//                            $variationArray = [];
//                        }
//
//                        if (is_string($variationArray)) {
//                            $variationArray = json_decode($variationArray);
//                        }
//                        if (!is_array($variationArray)) {
//                            $variationArray = [$variationArray];
//                        }

//                        $data_to_return['add_to_cart'][] = [$product_id, $qty, $variationId, $variationArray];
//
                        WC()->cart->add_to_cart($product_id);
                    }

                    $data_to_return['success'] = true;
//                    $data_to_return['user_cart'] = $mstore_class->cart();

                } catch (Exception $e) {
                    $data_to_return['msg'] = 'There is an error in the process!';
                }

            } else {
                $data_to_return['msg'] = 'products list is required!';
            }

        } else {
            $data_to_return['msg'] = 'user is not valid!';
        }

        return new WP_REST_Response($data_to_return);
    }

}

new appsazDuplicateOrder();
