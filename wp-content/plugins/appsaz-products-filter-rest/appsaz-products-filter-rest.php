<?php
/*
Plugin Name: appsaz products filter rest api
Description: get products by filter rest API
Version: 1.0.0
Author: Amir Sasani
Author URI: http://appsaz.ir
License: GPL2
*/


class AppsazProductsFilterRest
{
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'my_register_route']);
    }

    function my_register_route()
    {
        register_rest_route(
            'as_wc/v3',
            'products',
            array(
                'methods' => 'GET',
                'callback' => [$this, 'as_woocommerce_get_products'],
            )
        );
    }

    function as_woocommerce_get_products(WP_REST_Request $request)
    {
        $data = $request->get_params();
        $page = $data['page'];
        $status = $data['status'];
        $type = $data['type'];
        $order = $data['order'];
        $orderby = $data['orderby'];
        $category = $data['category'];
        $min_price = $data['min_price'];
        $max_price = $data['max_price'];
        $search = $data['search'];

        $query_args = [];

        $limit = 10;
        if (empty($page) || !is_numeric($page)) {
            $page = 1;
        } else {
            $page = intval($page);
        }

        $query_args['posts_per_page'] = $limit;
        $query_args['offset'] = ($page - 1) * $limit;


        if (!empty($status)) {
            $query_args['post_status'] = $status;
        }

        if (!empty($type)) {
            $query_args['type'] = $type;
        }

        if (!empty($category)) {
            $query_args['tax_query'] = [
                [
                    'taxonomy' => 'product_cat',
                    'terms' => $category,
                ]
            ];
        }

        if (empty($order) || (!empty($order) && !in_array(strtolower($order), ['asc', 'desc']))) {
            $order = 'DESC';
        } else {
            $order = strtoupper($order);
        }

        if (!empty($orderby)) {

            if ($orderby == 'id') {
                $orderby = 'ID';
            }

            switch ($orderby) {
                case 'total_views':
                    $query_args['orderby'] = 'meta_value';
                    $query_args['meta_query'] = [
                        'relation' => 'OR',
                        ['key' => 'view_total_view', 'compare' => 'NOT EXISTS'],
                        ['key' => 'view_total_view', 'compare' => 'EXISTS'],
                    ];
                    break;

                case 'price':
                    $query_args['orderby'] = 'meta_value_num';
                    $query_args['meta_key'] = '_price';

                    break;

                default:
                    $query_args['orderby'] = $orderby;
                    break;
            }

            $query_args['order'] = $order;
        }

//        if (!empty($min_price) && is_numeric($min_price)) {
//
//            $query_args['meta_query'] = [
//                'key' => '_price',
//                'value' => $min_price,
//                'compare' => '>=',
//                'type' => 'NUMERIC'
//            ];
//
//        }

        if (!empty($max_price) && is_numeric($max_price) &&
            !empty($min_price) && is_numeric($min_price)) {

            $query_args['meta_query'] = array(
                array(
                    'key' => '_price',
                    'value' => array($min_price, $max_price),
                    'compare' => 'BETWEEN',
                    'type' => 'NUMERIC'
                ),
            );

        }

        if (!empty($search)) {
            $query_args['s'] = $search;
        }

        $query_args['fields'] = 'ids';
        $query_args['post_type'] = 'product';

        $query_args = apply_filters('as_appsaz_woocommerce_products_filter_rest_api__wp_query_args', $query_args, $data);

//        echo print_r($query_args, true);
//        die();

        $products_query = new WP_Query($query_args);
        $products_ids = $products_query->posts;

        $data_to_return = [];

        $products = array_map([$this, 'get_product_object'], $products_ids);

        $data_to_return = $products;


        return new WP_REST_Response($data_to_return);
    }

    private function get_product_object($product_id)
    {
        $product = wc_get_product($product_id);
        $_out = [];

        $_out['id'] = $product->get_id();
        $_out['name'] = $product->get_name();
        $_out['slug'] = $product->get_slug();
        $_out['price'] = $product->get_price();
        $_out['regular_price'] = $product->get_regular_price();
        $_out['sale_price'] = $product->get_sale_price();
        $_out['on_sale'] = $product->is_on_sale();
        $_out['stock_status'] = $product->is_in_stock() ? 'instock' : 'outofstock';
        $_out['stock_quantity'] = $product->get_stock_quantity();
        $_out['categories'] = $this->get_product_categories($product);
        $_out['images'] = $this->get_product_images($product);

        return apply_filters('as_appsaz_woocommerce_products_filter_rest_api__product_object', $_out, $product);
    }

    private function get_product_categories(WC_Product $product)
    {
        $category_ids = $product->get_category_ids();

        return array_map(function ($cat_id) {
            $_out = [];

            $category = get_term($cat_id);

            $_out['id'] = $category->term_id;
            $_out['name'] = $category->name;
            $_out['slug'] = $category->slug;

            return $_out;
        }, $category_ids);
    }

    private function get_product_images(WC_Product $product)
    {
        $images = array();
        $attachment_ids = array();

        // Add featured image.
        if ($product->get_image_id()) {
            $attachment_ids[] = $product->get_image_id();
        }

        // Add gallery images.
        $attachment_ids = array_merge($attachment_ids, $product->get_gallery_image_ids());

        // Build image data.
        foreach ($attachment_ids as $attachment_id) {
            $attachment_post = get_post($attachment_id);
            if (is_null($attachment_post)) {
                continue;
            }

            $attachment = wp_get_attachment_image_src($attachment_id, 'full');
            if (!is_array($attachment)) {
                continue;
            }

            $images[] = array(
                'id' => (int)$attachment_id,
//                'date_created' => wc_rest_prepare_date_response($attachment_post->post_date, false),
//                'date_created_gmt' => wc_rest_prepare_date_response(strtotime($attachment_post->post_date_gmt)),
//                'date_modified' => wc_rest_prepare_date_response($attachment_post->post_modified, false),
//                'date_modified_gmt' => wc_rest_prepare_date_response(strtotime($attachment_post->post_modified_gmt)),
                'src' => current($attachment),
//                'name' => get_the_title($attachment_id),
//                'alt' => get_post_meta($attachment_id, '_wp_attachment_image_alt', true),
            );
        }

        return $images;
    }
}

new AppsazProductsFilterRest();
