<?php
/**
 * Developer : WooCommerce.ir
 * version 4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

$path = WOOI_PLUGIN_DIR . 'template/stock/1/header.php';

require apply_filters( 'wooi_stock_header_path', $path );

$path = WOOI_PLUGIN_DIR . 'template/stock/1/body.php';

require apply_filters( 'wooi_stock_body_path', $path );

$path = WOOI_PLUGIN_DIR . 'template/stock/1/footer.php';

require apply_filters( 'wooi_stock_footer_path', $path );

