<?php
/**
 * Developer : WooCommerce.ir
 * version 4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Class Woocommerce_Invoice
 */
class Woocommerce_Invoice {

	private static $_instance = null;

	public static function instance( $file ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $file );
		}

		return self::$_instance;
	}

	public function __construct() {
		add_action( 'init', array( $this, 'init' ) );
		add_action( 'add_meta_boxes', array( $this, 'order_meta_box' ) );

		add_filter( 'manage_edit-shop_order_columns', array( $this, 'add_order_column' ), 20 );
		add_action( 'manage_shop_order_posts_custom_column', array( $this, 'order_column_content' ) );

		add_filter( 'bulk_actions-edit-shop_order', array( $this, 'my_bulk_actions_edit_shop_order' ) );
		add_filter( 'woocommerce_my_account_my_orders_actions', array( $this, 'my_account_my_orders_actions' ), 10, 2 );
	}

	public function init() {

		$actions = array(
			'wooi_invoice',
			'wooi_ticket',
			'wooi_stock',
			'wooi_thermal',
		);

		if ( isset( $_GET['action'], $_GET['invoice_id'] ) && $_GET['action'] == 'print' ) {

			$ID = intval( $_GET['invoice_id'] );

			/** @var WC_Order $order */
			$order = new WC_Order( $ID );

			WOOI()->add_woocommerce_string_filter();

			if ( $order->get_customer_id() == get_current_user_id() ) {

				$IDs = [ $ID ];

				include 'invoice.php';

				die();
			}

		}

		if ( ! ( isset( $_GET['action'], $_GET['post'] ) && in_array( $_GET['action'], $actions ) ) ) {
			return false;
		}

		$IDs = $this->get_IDs();

		foreach ( $IDs as $ID ) {
			if ( ! current_user_can( 'read_shop_order', $ID ) ) {
				return false;
			}
		}

		WOOI()->add_woocommerce_string_filter();

		if ( $_GET['action'] == 'wooi_invoice' ) {
			include 'invoice.php';
		}

		if ( $_GET['action'] == 'wooi_ticket' ) {
			include 'ticket.php';
		}

		if ( $_GET['action'] == 'wooi_stock' ) {
			include 'stock.php';
		}

		if ( $_GET['action'] == 'wooi_thermal' ) {
			include 'thermal.php';
		}

		die();
	}

	public function order_meta_box() {
		add_meta_box( 'wooi', 'پرینت', array( $this, 'order_meta_box_callback' ), 'shop_order', 'side' );
	}

	public function order_meta_box_callback( $post, $args ) {
		?>
        <div style="text-align: center;">
            <a href="<?php echo admin_url( 'edit.php?post=' . $post->ID . '&action=wooi_invoice' ); ?>" target="_blank"
               title="فاکتور">
                <button class="button button-primary" type="button">چاپ فاکتور</button>
            </a><br><br>
            <a href="<?php echo admin_url( 'edit.php?post=' . $post->ID . '&action=wooi_ticket' ); ?>" target="_blank"
               title="برچسب">
                <button class="button button-primary" type="button">چاپ برچسب</button>
            </a><br><br>
            <a href="<?php echo admin_url( 'edit.php?post=' . $post->ID . '&action=wooi_thermal' ); ?>" target="_blank"
               title="پرینت حرارتی">
                <button class="button button-primary" type="button">پرینتر حرارتی</button>
            </a>
        </div>
		<?php
	}

	public function add_order_column( $columns ) {

		$new_columns = array();

		foreach ( $columns as $column_name => $column_info ) {

			$new_columns[ $column_name ] = $column_info;

			if ( 'order_total' === $column_name ) {
				$new_columns['print'] = 'چاپ';
			}
		}

		return $new_columns;
	}

	public function order_column_content( $column ) {
		global $post;

		if ( $column != 'print' ) {
			return false;
		}

		echo '<a href="' . admin_url( 'edit.php?post=' . $post->ID . '&action=wooi_invoice' ) . '" target="_blank" title="فاکتور"><mark class="order-status"><span><span class="dashicons dashicons-media-spreadsheet" style="line-height: 33px;"></span></span></mark></a>&nbsp;';
		echo '<a href="' . admin_url( 'edit.php?post=' . $post->ID . '&action=wooi_ticket' ) . '" target="_blank" title="برچسب"><mark class="order-status status-processing"><span><span class="dashicons dashicons-tag" style="line-height: 33px;"></span></span></mark></a>&nbsp;';
		echo '<a href="' . admin_url( 'edit.php?post=' . $post->ID . '&action=wooi_thermal' ) . '" target="_blank" title="پرینت حرارتی"><mark class="order-status status-completed"><span><span class="dashicons dashicons-text-page" style="line-height: 33px;"></span></span></mark></a>';
	}

	public function my_bulk_actions_edit_shop_order( $actions ) {

		$actions['wooi_invoice'] = 'چاپ فاکتور';
		$actions['wooi_ticket']  = 'چاپ برچسب';
		$actions['wooi_stock']   = 'چاپ لیست انبار';

		return $actions;
	}

	public function my_account_my_orders_actions( $actions, $order ) {

		$actions['wooi'] = array(
			'url'  => add_query_arg( array(
				'action'     => 'print',
				'invoice_id' => $order->get_id()
			), $order->get_view_order_url() ),
			'name' => 'مشاهده فاکتور',
		);

		return $actions;
	}

	function add_woocommerce_string_filter() {
		$props = array(
			'get_billing_first_name',
			'get_billing_last_name',
			'get_billing_postcode',
			'get_billing_phone',
			'get_billing_address_1',
		);

		foreach ( $props as $prop ) {
			add_filter( 'woocommerce_order_' . $prop, 'wooi_fa' );
			add_filter( 'woocommerce_order_' . str_replace( 'billing', 'shipping', $prop ), 'wooi_fa' );
		}

		add_filter( 'woocommerce_order_number', 'wooi_fa' );

		$options = array(
			'woocommerce_store_name',
			'woocommerce_store_city',
			'woocommerce_store_postcode',
			'woocommerce_store_phone',
			'woocommerce_store_address',
		);

		foreach ( $options as $option ) {
			add_filter( 'option_' . $option, 'wooi_fa' );
		}

	}

	function get_IDs() {

		if ( ! isset( $_GET['post'] ) ) {
			return false;
		}

		if ( ! is_array( $_GET['post'] ) ) {
			$IDs = array_filter( explode( ',', $_GET['post'] ) );
		} else {
			$IDs = $_GET['post'];
		}

		return array_filter( array_unique( array_map( 'intval', $IDs ) ) );

	}
}
