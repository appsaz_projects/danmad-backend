<?php
/**
 * Developer : WooCommerce.ir
 * version 4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

$path = WOOI_PLUGIN_DIR . 'template/invoice/1/header.php';

require apply_filters( 'wooi_invoice_header_path', $path );

$logo = '';

$attachment_id = PW()->get_options( 'wooi_store_logo' );

if ( $image_attributes = wp_get_attachment_image_src( $attachment_id, 'full' ) ) {

	$logo = $image_attributes[0];

}

foreach ( $IDs as $ID ) :

	/** @var WC_Order $order */
	$order = wc_get_order( $ID );

	$order_data = $order->get_data();

	$date = $order_data['date_created']->date_i18n( "Y/m/d" );

	$different_address = false;

	$fields = array(
		'first_name',
		'last_name',
		'state',
		'city',
		'postcode',
		'address_1',
	);

	foreach ( $fields as $field ) {

		$billing  = $order->{'get_billing_' . $field}();
		$shipping = $order->{'get_shipping_' . $field}();

		if ( in_array( $field, [ 'state', 'city' ] ) ) {
			$different_address = wooi_state_city_name( $billing ) != wooi_state_city_name( $shipping );
		} else {
			$different_address = $billing != $shipping;
		}

		$different_address = $different_address && ! empty( $shipping );

		if ( $different_address ) {
			break;
		}
	}

	$path = WOOI_PLUGIN_DIR . 'template/invoice/1/body.php';

	require apply_filters( 'wooi_invoice_body_path', $path, $order );

endforeach;

$path = WOOI_PLUGIN_DIR . 'template/invoice/1/footer.php';

require apply_filters( 'wooi_invoice_footer_path', $path );