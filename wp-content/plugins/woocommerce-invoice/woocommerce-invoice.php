<?php
/*
Plugin Name: فاکتور ووکامرس
Plugin URI: https://woocommerce.ir
Description: صدور و چاپ فاکتور برای سفارشات ثبت شده در ووکامرس ، امکان صدور فاکتور ویژه پرینترهای نوری و پرینترهای لیزری. کدنویسی و توسعه توسط <a href="https://woocommerce.ir" target="_blank">ووکامرس فارسی</a>
Version: 4.0.1
Author: ووکامرس فارسی
Author URI: https://woocommerce.ir
WC requires at least: 3.0.0
WC tested up to: 4.0.1
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! defined( 'WOOI_VERSION' ) ) {
	define( 'WOOI_VERSION', '4.0.1' );
}

if ( ! defined( 'WOOI_PLUGIN_DIR' ) ) {
	define( 'WOOI_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'WOOI_PLUGIN_URL' ) ) {
	define( 'WOOI_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'WOOI_PLUGIN_FILE' ) ) {
	define( 'WOOI_PLUGIN_FILE', __FILE__ );
}
if ( ! function_exists( 'is_plugin_active' ) ) {
	require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
}
if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) || ! is_plugin_active( 'persian-woocommerce/woocommerce-persian.php' ) ) {
	return false;
}

include 'includes/woocommerce-invoice.php';
include 'includes/tools.php';
include 'includes/functions.php';

function WOOI() {
	return Woocommerce_Invoice::instance( __FILE__ );
}

$GLOBALS['WOOI'] = WOOI();

register_activation_hook( __FILE__, array( 'Woocommerce_Invoice', 'install' ) );
