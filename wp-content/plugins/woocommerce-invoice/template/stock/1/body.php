<?php defined( 'ABSPATH' ) || exit; ?>
<style>
    body{
        font-family:<?php echo PW()->get_options( 'wooi_font' ); ?>!important;
        color:<?php echo PW()->get_options( 'wooi_font_color' ); ?>;
    }
    .content.factor th {
        background:<?php echo PW()->get_options( 'wooi_bg_color' ); ?>;
    }
    
    
</style>
<table style="width:100%">
    <thead>
    <tr>
        <th>نام مشتری</th>
        <th>شماره سفارش</th>
        <th>تاریخ سفارش</th>
        <th>محصولات خریداری شده</th>
        <th>گزارش وضعیت سفارش</th>
    </tr>
    </thead>

	<?php
	foreach ( $IDs as $ID ) {

		$order = wc_get_order( $ID );

		$order_data = $order->get_data();

		$items = $order->get_items();

		?>
        <tr>
            <td>
                <?php if ( $order->get_formatted_shipping_address() ) { ?>
				<?php echo $order->get_shipping_first_name() . " " . $order->get_shipping_last_name(); ?>
                <br><?php echo $order->get_billing_phone(); ?>
                <?php } else { ?>
				<?php echo $order->get_billing_first_name() . " " . $order->get_billing_last_name(); ?>
                <br><?php echo $order->get_billing_phone(); ?>
                <?php } ?>
            </td>

            <td> <?php echo $order->get_order_number(); ?></td>

            <td><?php echo $order_data['date_created']->date_i18n( "Y/m/d" ); ?></td>
            <td style="padding: 0;margin: 0;">
                <table style="width: 100%;padding: 0;margin: 0;border: none;">
					<?php foreach ( $items as $item ) :
						$sku = get_post_meta( $item->get_product_id(), '_sku', true );
						$sku = $sku ? $sku : $item->get_product_id();
						?>

                        <tr>
                            <td style="border: 1px solid;"><?php echo wooi_fa( $item->get_quantity() ); ?> عدد</td>
                            <td style="border: 1px solid;"><?php echo $item['name']; ?></td>
                            <td style="border: 1px solid;"><?php echo $sku; ?></td>
                        </tr>
					<?php
					endforeach;
					?>
                </table>
            </td>
            <td><?php echo $order->get_status(); ?></td>
        </tr>

		<?php
	}
	?>
</table>