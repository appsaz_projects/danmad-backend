<?php defined( 'ABSPATH' ) || exit; ?>
<style>
    body {
        font-family: <?php echo PW()->get_options( 'wooi_font' ); ?> !important;
        color: <?php echo PW()->get_options( 'wooi_font_color' ); ?>;
    }

    .content.factor th {
        background: <?php echo PW()->get_options( 'wooi_bg_color' ); ?>;
    }

</style>
<div class="content factor">
    <div class="header">
        <div class="logo"><img src="<?php echo $logo; ?>" alt=" ">&nbsp;</div>
        <div class="text">فاکتور</div>
        <div class="info">
            <p>شماره فاکتور: <?php echo $order->get_order_number(); ?></p>
            <p><img src="<?php echo wooi_barcode( $order->get_id() ); ?>" alt=" "></p>
            <p>تاریخ: <?php echo $date; ?></p>
        </div>
    </div>
    <table>
        <tr>
            <th width="50%">فروشنده</th>
            <th width="50%">خریدار</th>
        </tr>
        <tr>
            <td>
                <p>
                    <b>فروشنده: </b>
					<?php echo PW()->get_options( 'wooi_store_name' ); ?>
                    &emsp;&emsp;
                    <b>استان: </b>
					<?php echo PW()->get_options( 'wooi_store_state' ); ?>
                    <b>شهر: </b>
					<?php echo PW()->get_options( 'wooi_store_city' ); ?>
                    <b>کد پستی: </b>
					<?php echo wooi_fa( PW()->get_options( 'wooi_store_postcode' ) ); ?>
                    <b>تلفن: </b>
					<?php echo wooi_fa( PW()->get_options( 'wooi_store_phone' ) ); ?>
                    <b <?php wooi_show_item( PW()->get_options( 'wooi_store_economic_code' ) ); ?>>کد اقتصادی: </b>
					<?php echo wooi_fa( PW()->get_options( 'wooi_store_economic_code' ) ); ?>
                    <b <?php wooi_show_item( PW()->get_options( 'wooi_store_registration_number' ) ); ?>>شماره ثبت: </b>
					<?php echo wooi_fa( PW()->get_options( 'wooi_store_registration_number' ) ); ?>
                </p>
                <p>
                    <b>نشانی: </b>
					<?php echo wooi_fa( PW()->get_options( 'wooi_store_address' ) ); ?>
                </p>
            </td>

            <td>
                <p>
                    <b>خریدار: </b>
					<?php echo $order->get_billing_first_name() . " " . $order->get_billing_last_name(); ?>
                    &emsp;&emsp;
                    <b>استان: </b>
					<?php echo wooi_state_city_name( $order->get_billing_state() ); ?>
                    <b>شهر: </b>
					<?php echo wooi_state_city_name( $order->get_billing_city() ); ?>
                    <b>کد پستی: </b>
					<?php echo $order->get_billing_postcode(); ?>
                    <b>شماره تماس: </b>
					<?php echo $order->get_billing_phone(); ?>
                </p>
                <p>
                    <b>نشانی: </b>
		            <?php echo $order->get_billing_address_1(); ?>
		            <?php echo $order->get_billing_address_2(); ?>
                </p>
            </td>
        </tr>
        <tr <?php wooi_show_item( $different_address ); ?>>
            <th colspan="2">حمل و نقل</th>
        </tr>
        <tr <?php wooi_show_item( $different_address ); ?>>
            <td colspan="2">
                <p>
                    <b>خریدار: </b> <?php echo $order->get_shipping_first_name() . " " . $order->get_shipping_last_name(); ?>
                    &emsp;&emsp;
                    <b>استان: </b>
					<?php echo wooi_state_city_name( $order->get_shipping_state() ); ?>
                    <b>شهر: </b>
					<?php echo wooi_state_city_name( $order->get_shipping_city() ); ?>
                    <b>کد پستی: </b>
					<?php echo $order->get_shipping_postcode(); ?>
                    <b>نشانی: </b>
					<?php echo $order->get_shipping_address_1(); ?>
					<?php echo $order->get_shipping_address_2(); ?>
                </p>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <th>ردیف</th>
            <th>کد کالا</th>
            <th style="min-width: 300px;">شرح کالا یا خدمات</th>
            <th>تعداد</th>
            <th>مبلغ واحد<br/>(<?php echo get_woocommerce_currency_symbol(); ?>)</th>
            <th>مبلغ کل<br/>(<?php echo get_woocommerce_currency_symbol(); ?>)</th>
        </tr>
		<?php

		$row = 1;

		foreach ( $order->get_items() as $item_key => $item_values ) :

			$data = $item_values->get_data();

			$sku = get_post_meta( $item_values->get_product_id(), '_sku', true );
			$sku = $sku ? $sku : $item_values->get_product_id();
			?>
            <tr>
                <td class="centerp"><?php echo wooi_fa( $row ); ?></td>
                <td class="centerp"><?php echo wooi_fa( $sku ); ?></td>
                <td>
					<?php echo $data['name']; ?>
					<?php wc_display_item_meta( $item_values, [
						'before'    => '<p>',
						'after'     => '</p>',
						'separator' => ' | ',
					] ); ?>
                </td>
                <td class="centerp"><?php echo wooi_fa( $data['quantity'] ); ?></td>
                <td class="centerp"><?php echo wooi_price( $data['total'] / $data['quantity'] ); ?></td>
                <td class="centerp"><?php echo wooi_price( $data['total'] ); ?></td>
            </tr>
			<?php

			$row ++;
		endforeach;

		?>
        <!----->
		<?php if ( $order->get_total_discount() > 0 ) { ?>
            <tr>
                <td colspan="4" class="leftp">تخفیف:</td>
                <td colspan="2" class="centerp"><?php echo wooi_price( $order->get_total_discount() ); ?></td>
            </tr>
		<?php }
		if ( $order->get_shipping_total() > 0 ) { ?>
            <tr>
                <td colspan="4" class="leftp">هزینه ارسال - <?php echo $order->get_shipping_method(); ?>:</td>
                <td colspan="2" class="centerp"><?php echo wooi_price( $order->get_shipping_total() ); ?></td
            </tr>
		<?php }
		if ( $order->get_total_tax() > 0 ) { ?>
            <tr>
                <td colspan="4" class="leftp">مالیات:</td>
                <td colspan="2" class="centerp"><?php echo wooi_price( $order->get_total_tax() ); ?></td>
            </tr>
		<?php } ?>
        <tr>
            <td colspan="4" class="leftp">روش پرداخت:</td>
            <td colspan="2" class="centerp"><?php echo $order->get_payment_method_title(); ?></td>
        </tr>
        <tr>
            <th colspan="3">مجموع مبلغ نهایی</th>
            <th colspan="3"
                class="centerp"><?php echo wooi_price( $order->get_total() ); ?><?php echo get_woocommerce_currency_symbol(); ?></th>
        </tr>
    </table>
    <div class="emza centerp">
		<?php echo wpautop( PW()->get_options( 'wooi_store_description' ) ); ?>
    </div>
</div>