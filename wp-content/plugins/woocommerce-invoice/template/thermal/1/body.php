<?php defined( 'ABSPATH' ) || exit; ?>
<style>
    body {
        font-family: <?php echo PW()->get_options( 'wooi_font' ); ?> !important;
        color: <?php echo PW()->get_options( 'wooi_font_color' ); ?>;
        font-size: 11px;
    }

    .content.factor th {
        background: <?php echo PW()->get_options( 'wooi_bg_color' ); ?>;
        font-size: 13px;
    }

    @page {
        margin: 0;
    }

</style>
<div class="thermal content factor">
    <div class="header">
        <img src="<?php echo $logo; ?>" style="max-width: 100%;display:flex;margin:0 auto" alt="">
        <h2 class="centerp"><?php echo PW()->get_options( 'wooi_store_name' ); ?></h2>

        <p>شماره فاکتور: <?php echo $order->get_order_number(); ?> - تاریخ: <?php echo $date; ?>
        <p>
            <?php echo PW()->get_options( 'wooi_store_name' ); ?>
            | <?php echo wooi_fa( PW()->get_options( 'wooi_store_address' ) ); ?>
            | <?php echo wooi_fa( PW()->get_options( 'wooi_store_phone' ) ); ?></p>
    </div>
    <table width="300">

        <tr>
            <td>
                <p>
                    <b>خریدار: </b>
					<?php echo $order->get_billing_first_name() . " " . $order->get_billing_last_name(); ?>

                </p>
                <p>
                    <b>نشانی: </b> <?php echo $order->get_billing_address_1(); ?> <?php echo $order->get_billing_address_2(); ?>
                    <b>کد پستی: </b><?php echo $order->get_billing_postcode(); ?>
                    <b>تلفن: </b><?php echo $order->get_billing_phone(); ?></p>
            </td>
        </tr>
        <tr <?php wooi_show_item( $different_address ); ?>>
            <th>حمل و نقل</th>
        </tr>
        <tr <?php wooi_show_item( $different_address ); ?>>
            <td>
                <p>
                    <b>خریدار: </b>
					<?php echo $order->get_shipping_first_name() . " " . $order->get_shipping_last_name(); ?>

                </p>
                <p>
                    <b>نشانی: </b> <?php echo $order->get_shipping_address_1(); ?> <?php echo $order->get_shipping_address_2(); ?>
                    <b>کد پستی: </b><?php echo $order->get_shipping_postcode(); ?></p>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <th>عنوان</th>
            <th>تعداد</th>
            <th>مبلغ واحد<br/>(<?php echo get_woocommerce_currency_symbol(); ?>)</th>
            <th>مبلغ کل<br/>(<?php echo get_woocommerce_currency_symbol(); ?>)</th>
        </tr>
		<?php

		$total     = 0;
		$row       = 1;

		foreach ( $order->get_items() as $item_key => $item_values ) :

			$data = $item_values->get_data();
			$total += $data['total'];

			$sku = get_post_meta( $item_values->get_product_id(), '_sku', true );
			$sku = $sku ? $sku : $item_values->get_product_id();
			?>
            <tr>
                <td><?php echo $data['name']; ?></td>
                <td class="centerp"><?php echo wooi_fa( $data['quantity'] ); ?></td>
                <td class="centerp"><?php echo wooi_price( $data['total'] / $data['quantity'] ); ?></td>
                <td class="centerp"><?php echo wooi_price( $data['total'] ); ?></td>
            </tr>
			<?php

			$row ++;
		endforeach;

		$total += $order->get_shipping_total();
		?>
        <!----->
		<?php if ( $order->get_total_discount() > 0 ) { ?>
            <tr>
                <td colspan="2" class="leftp">تخفیف:</td>
                <td colspan="2" class="centerp"><?php echo wooi_price( $order->get_total_discount() ); ?></td>
            </tr>
		<?php }
		if ( $order->get_shipping_total() > 0 ) { ?>
            <tr>
                <td colspan="2" class="leftp">هزینه ارسال - <?php echo $order->get_shipping_method(); ?>:</td>
                <td colspan="2" class="centerp"><?php echo wooi_price( $order->get_shipping_total() ); ?></td
            </tr>
		<?php }
		if ( $order->get_total_tax() > 0 ) { ?>
            <tr>
                <td colspan="2" class="leftp">مالیات:</td>
                <td colspan="2" class="centerp"><?php echo wooi_price( $order->get_total_tax() ); ?></td>
            </tr>
		<?php } ?>
        <tr>
            <td colspan="2" class="leftp">روش پرداخت:</td>
            <td colspan="2" class="centerp"><?php echo $order->get_payment_method_title(); ?></td>
        </tr>
        <tr>
            <th colspan="2">مجموع مبلغ نهایی</th>
            <th colspan="2"
                class="centerp"><?php echo wooi_price( $total ); ?><?php echo get_woocommerce_currency_symbol(); ?></th>
        </tr>
    </table>
    <div class="emza centerp">
		<?php echo wpautop( PW()->get_options( 'wooi_store_description' ) ); ?>
    </div>
</div>