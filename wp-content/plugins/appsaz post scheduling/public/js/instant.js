jQuery(document).ready(function(){
    var loading="";
    loading +=" <div class='spinner'>";
    loading +=" <div class='double-bounce1'></div>";
    loading +=" <div class='double-bounce'></div>";
    loading +=" </div>";
    jQuery("#day_date").change(function() {
        var value=jQuery("#day_date").val();
        jQuery("#timepart_field").remove();
        jQuery.ajax({
            type: "POST",
            dataType: "json",
            data: {
                'time':value
            },
            url: '/wp-json/get/v1/shipping_time',
            beforeSend:function(){
                jQuery("#daypart_field").append(loading);
                jQuery("#timepart_field").remove();
            },
            success: function (response) {
              response=JSON.parse(response);
                var html = "";
                if(response) {
                    html += '<p class="form-row wps-drop validate-required" id="timepart_field" data-priority="">' +
                        '<label for="timepart" class="">انتخاب زمان&nbsp;<abbr class="required" title="ضروری">*</abbr></label>' +
                        '<span class="woocommerce-input-wrapper"><select name="timepart" id="timepart" class="select " data-placeholder="">';
                    for (const prop in response) {
                        html += "<option value='" + `${response[prop]}` + "'>" + `${response[prop]}` + "</option>";

                    }
                    html += '</select></span></p>';
                }
                else{
                    html +='<p class="form-row wps-drop validate-required" id="timepart_field" data-priority="">';
                    html +='<span class="woocommerce-input-wrapper" style="color:red">برای روز انتخابی زمان ارسالی وجود ندارد لطفا روز دیگری را انتخاب کنید.</span>';
                    html +='</p>';
                }
                jQuery('#time-delevery-tarebox').append(html);

            },
            complete:function(){
                jQuery(".spinner").remove();
            },
        });
    });
})
