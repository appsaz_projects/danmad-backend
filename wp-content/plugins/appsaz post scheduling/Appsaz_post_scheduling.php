<?php

/**
 * @package  AppsazAdminInterface
 */
/*
Plugin Name: زمانبندی ارسال
Plugin URI: https://appsaz.ir/
Description:
Version: 2.1.0
Author: mhdighsmi
Author URI: https://appsaz.ir
License: GPLv2 or later
Text Domain: APS
*/
defined( 'ABSPATH' ) or die( 'Hey, what are you doing here? You silly human!' );

define("APS_PATH",plugin_dir_path(__FILE__));
define("APS_URL",plugin_dir_url(__FILE__));


if(file_exists(dirname(__FILE__).'/vendor/autoload.php')){
    include dirname(__FILE__).'/vendor/autoload.php';
}
use Inc\Menu_page;
use Inc\Enqueue_Aps;
use Inc\Api;
use Inc\Activate_Aps;
use Inc\Deactivate_Aps;
class Appsaz_post_scheduling
{
    function __construct()
    {
        add_action( 'update_option_woo_post_scheduling', array($this,'aps_woo_post_scheduling_after_save'), 10, 2 );

        add_action( 'post_scheduling_day_time',array($this, 'aps_post_scheduling_day_time' ));

        add_action('woocommerce_checkout_update_order_meta', array($this,'aps_update_order_meta'));

        if (!wp_next_scheduled('post_scheduling_day_time')) {
            wp_schedule_event(strtotime('22:55:00'), 'daily', 'post_scheduling_day_time');
        }
     //   add_filter( 'woocommerce_checkout_fields' ,array($this,'aps_override_checkout_fields'), 99);

       // add_action('woocommerce_before_order_notes', array($this,'aps_add_select_checkout_field'));

    add_action( 'woocommerce_admin_order_data_after_billing_address', array($this,'wps_select_checkout_field_display_admin_order_meta'), 10, 1 );


    }
    public function wps_select_checkout_field_display_admin_order_meta($order){
        echo '<p><strong>'.__('روز و ساعت انتخابي').':</strong> ' . get_post_meta( $order->id, 'deliveryDate', true ) . '</p>';
    }
    public  function aps_add_select_checkout_field( $checkout )
    {
        echo '<div id="time-delevery-tarebox"><h5>' . __('shipping time','APS') . '</h5>';
        $options = get_option( 'woo_post_scheduling' );
        $daypart=[];
        for($i=0;$i<count($options);$i++){
            $daypart[$i] = date("l j F Y", strtotime("+".$i." day"));

        }

        woocommerce_form_field('daypart', array(
            'type' => 'select',
            'class' => array('wps-drop'),
            'required' => true,
            'label' => __('انتخاب روز', 'woocommerce'),
            'options' =>$daypart
        ),
            $checkout->get_value('daypart'));
        echo "</div>";
    }

    public function aps_override_checkout_fields( $fields ) {

        $only_virtual = true;
        foreach( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            // Check if there are non-virtual products
            if ( ! $cart_item['data']->is_virtual() ) $only_virtual = false;
        }
        if( $only_virtual ) {
            add_filter( 'woocommerce_enable_order_notes_field', '__return_false', 9999 );
            remove_action('woocommerce_before_order_notes', 'wps_add_select_checkout_field');
        }
        return $fields;
    }

    function aps_woo_post_scheduling_after_save(){
        $options = get_option( 'woo_post_scheduling' );
        update_option( 'woo_post_scheduling_defualt', $options );
    }
    function aps_post_scheduling_day_time(){
        $now_day= date('l', strtotime(date('Y-m-d')));
        $days=[
            '0'=>'Saturday',
            '1'=>'Monday',
            '2'=>'Tuesday',
            '3'=>'Wednesday',
            '4'=>'Thursday',
            '5'=>'Friday',
            '6'=>'Sunday',
        ];
        if (false !== $k = array_search($now_day, $days)) {
           $key=$k;
        } 
        
        $options= get_option( 'woo_post_scheduling' );
        $options_default= get_option( 'woo_post_scheduling_defualt' );
         $options[$key]['post_count'] =  $options_default[$key]['post_count'];
        update_option( 'woo_post_scheduling', $options );
    }

    function aps_update_order_meta($order_id){
        if ($_POST['daypart']);
        {
            $daypart = $_POST['daypart'];
            update_post_meta($order_id, 'deliveryDate', esc_attr($daypart).'('.$_POST['timepart'].')');

            $options = get_option('woo_post_scheduling');
            $options_default = get_option('woo_post_scheduling_defualt');
            for($i=0;$i<sizeof($options[$_POST['dayIndex']]['day_period']);$i++){
                if ($options[$_POST['dayIndex']]['day_period'][$i] == $_POST['timepart']) {
                    $options[$_POST['dayIndex']]['post_count'][$i] = ($options[$_POST['dayIndex']]['post_count'][$i] ) - 1;
                    $options_default[$_POST['dayIndex']]['post_count'][$i] = ($options_default[$_POST['dayIndex']]['post_count'][$i]);
                }
                
            }
            update_option('woo_post_scheduling', $options);
            update_option('woo_post_scheduling_defualt', $options_default);

        }
    }

}
new Appsaz_post_scheduling();
$Activate_Aps=new Activate_Aps;
$Deactivate_Aps=new Deactivate_Aps;
register_activation_hook(__FILE__,array('Activate_Aps','active'));

register_deactivation_hook(__FILE__,array('Deactivate_Aps','deactive'));
new Menu_page();
new Enqueue_Aps();
new Api();
