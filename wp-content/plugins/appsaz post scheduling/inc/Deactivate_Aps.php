<?php
/**
 * Created by PhpStorm.
 * User: mhdighsmi
 * Date: 3/29/2020
 * Time: 2:35 AM
 */

namespace Inc;


class Deactivate_Aps
{
    function __construct()
    {

    }
    public function deactive(){
        wp_clear_scheduled_hook( 'post_scheduling_day_time' );
    }

}