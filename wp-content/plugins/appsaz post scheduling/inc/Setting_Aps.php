<?php
namespace Inc;
if(!class_exists('Setting_Aps')) {


    class Setting_Aps
    {
        private $days;
        public function __construct()
        {
         //   wp_die(date('l', strtotime(date('Y-m-d'))));
            $this->days=[
                '0'=>'Saturday',
                '1'=>'Monday',
                '2'=>'Tuesday',
                '3'=>'Wednesday',
                '4'=>'Thursday',
                '5'=>'Friday',
                '6'=>'Sunday',
            ];
            add_action('admin_init', array($this,'aps_register_settings'));
            add_action( 'aps_settings_tab',array($this, 'aps_all_time_send_tab') );
            add_action( 'aps_settings_content', array($this,'aps_all_time_send_render_options_page'));
        }

        public function aps_register_settings(){
            register_setting('woo_post_scheduling', 'woo_post_scheduling', 'woo_post_scheduling_validate');
        }

        function aps_all_time_send_tab(){
            global $aps_active_tab; ?>
                <a href="<?php echo admin_url( 'edit.php?post_type=product&page=post-scheduling&tab=post-schulde-time' ); ?>" class="nav-tab" <?php echo $aps_active_tab == 'post-schulde-time' ? 'nav-tab-active' : '';?>><?php _e( 'Normal posting', 'APS' ); ?> </a>
            </h2>
            <?php
        }

        function aps_all_time_send_render_options_page()
        {
            global $aps_active_tab;
            if ('post-schulde-time' != $aps_active_tab)
                return;
            ?>
            <div class="wrap">
            <form action="options.php" method="post"><?php
            settings_fields('woo_post_scheduling');
            do_settings_sections(__FILE__);
            $options = get_option('woo_post_scheduling');
            ?>
            <div class="field_wrapper">
            <?php
            if ($options) {
                for ($i = 0; $i < sizeof($options); $i++) {
                    ?>
                    <div class="field_wrapper_day" data-id="<?php echo $i; ?>">
                        <?php for($j=0;$j<count($this->days);$j++) {
                            if($j==$i) {
                                ?>
                                <span class="day-count"><?php echo $this->days[$j]; ?></span>
                                <?php
                            }
                        }
                        if (isset($options[$i])) {
                            for ($j = 0; $j <= count($options[$i]); $j++) {
                                if ($options[$i]['day_period'][$j]) {
                                    ?>
                                    <div id="<?php echo $i; ?>">
                                        <input type="text" placeholder="<?php _e('period time','APS');?>"
                                               name="woo_post_scheduling[<?php echo $i; ?>][day_period][]"
                                               value="<?php echo $options[$i]['day_period'][$j] ?>"/><input
                                            type="number" placeholder="<?php _e('count post','APS');?>"
                                            name="woo_post_scheduling[<?php echo $i; ?>][post_count][]"
                                            value="<?php echo $options[$i]['post_count'][$j] ?>"/>
                                        <a href="javascript:void(0);" class="remove_day_post"> <span
                                                class="ab-icon">-</span></a>
                                    </div>
                                <?php }
                            }?>
                            <div id="<?php echo $i; ?>">
                                <input type="text" placeholder="<?php _e('period time','APS');?>"
                                       name="woo_post_scheduling[<?php echo $i; ?>][day_period][]"
                                       value=""/><input
                                    type="number" placeholder="<?php _e('count post','APS');?>"
                                    name="woo_post_scheduling[<?php echo $i; ?>][post_count][]"
                                    value=""/>
                                <a href="javascript:void(0);" class="add_day_post" title="Add field">
                                    <span class="ab-icon">+</span></a>
                                <a href="javascript:void(0);" class="remove_day_post"> <span
                                        class="ab-icon">-</span></a>
                            </div>
                     <?php   } ?>
                        <div class="btn-add-remove">
                            <a href="javascript:void(0);" class="remove_day"> <span class="ab-icon">-</span></a>
                        </div>
                    </div>
                    <?php
                }
                      if(sizeof($options)<7) {
                ?>
                <div class="field_wrapper_day" data-id="<?php echo sizeof($options); ?>">
                    <?php for($j=0;$j<count($this->days);$j++) {
                        if(sizeof($options)==$j) {
                            ?>
                            <span class="day-count"><?php echo $this->days[$j]; ?></span>
                            <?php
                        }
                    }?>                    <div id="0">
                        <input type="text" placeholder="<?php _e('period time','APS');?>"
                               name="woo_post_scheduling[<?php echo sizeof($options); ?>][day_period][]" value=""/>
                        <input type="number"
                               name="woo_post_scheduling[<?php echo sizeof($options); ?>][post_count][]"
                               placeholder="<?php _e('count post','APS');?>" value=""/>
                        <a href="javascript:void(0);" class="add_day_post" title="Add field">
                            <span class="ab-icon">+</span></a>
                        <a href="javascript:void(0);" class="remove_day_post"> <span class="ab-icon">-</span></a>
                    </div>
                    <div class="btn-add-remove">
                    <a href="javascript:void(0);" class="add_day" title="Add field">
                        <span class="ab-icon">+</span></a>
                    <a href="javascript:void(0);" class="remove_day"> <span class="ab-icon">-</span></a>
                    </div>
                </div>
            <?php
            }
             } else {
                ?>
                <div class="field_wrapper_day" data-id="0">
                    <span class="day-count">0</span>
                    <div id="0">
                        <input type="text" placeholder="<?php _e('period time','APS');?>" name="woo_post_scheduling[0][day_period][]"
                               value=""/>
                        <input type="number" name="woo_post_scheduling[0][post_count][]" placeholder="<?php _e('count post','APS');?>"
                               value=""/>
                        <a href="javascript:void(0);" class="add_day_post" title="Add field">
                            <span class="ab-icon">+</span></a>
                        <a href="javascript:void(0);" class="remove_day_post"> <span class="ab-icon">-</span></a>
                    </div>
                    <div class="btn-add-remove">
                    <a href="javascript:void(0);" class="add_day" title="Add field">
                        <span class="ab-icon">+</span></a>
                    <a href="javascript:void(0);" class="remove_day"> <span class="ab-icon">-</span></a>
                    </div>
                </div>
               <?php }
                ?>
                </div>
                <?php submit_button(); ?>
                </form>
                </div>

                <?php
        }
        function aps_post_admin_page_callback(){

            global $aps_active_tab;
            $aps_active_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'post-schulde-time'; ?>

            <h2 class="nav-tab-wrapper">

                <?php
                do_action( 'aps_settings_tab' );
                ?>
            </h2>
            <?php
            do_action( 'aps_settings_content' );
        }
    }
}