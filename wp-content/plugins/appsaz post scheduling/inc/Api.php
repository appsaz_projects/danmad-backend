<?php
/**
 * Created by PhpStorm.
 * User: mhdighsmi
 * Date: 3/29/2020
 * Time: 1:31 AM
 */

namespace Inc;


class Api
{
    function __construct()
    {
        add_action('rest_api_init', array($this, 'aps_register_post_scheduling_rest'));
    }

    function aps_register_post_scheduling_rest()
    {

        register_rest_route(
            'post/v1',
            '/scheduling',
            array(
                'methods' => 'GET',
                'callback' => array($this, 'callback_aps_register_post_scheduling_rest'),
            )
        );
    }

    function callback_aps_register_post_scheduling_rest()
    {
        $options = get_option("woo_post_scheduling");
        return $options;

    }

}
