<?php
/**
 * Created by PhpStorm.
 * User: mhdighsmi
 * Date: 3/28/2020
 * Time: 6:10 PM
 */

namespace Inc;

class Menu_page
{
    private $setting_aps;
    function __construct()
    {
        $this->setting_aps=new Setting_Aps();
        add_action( 'admin_menu', array($this,'aps_register_my_custom_menu_page') );

    }
    function aps_register_my_custom_menu_page(){
        add_menu_page(
            __( 'Schedule time', 'APS' ),
            __( 'Schedule time', 'APS' ),
            'manage_options',
            'post-scheduling',
            array( $this->setting_aps,'aps_post_admin_page_callback'),
            'dashicons-dashboard',
            6
        );
    }

}