<?php
/**
 * @package  AppsazAdminInterface
 */
/*
Plugin Name: پنل مدیریت اپ ساز
Plugin URI: https://appsaz.ir/
Description: ایجاد صفحه اصلی سایت به صورت خیلی زیبا
Version: 2.0.5
Author: mhdighsmi
Author URI: https://appsaz.ir
License: GPLv2 or later
Text Domain: appsaz-admin
*/

defined( 'ABSPATH' ) or die( 'Hey, what are you doing here? You silly human!' );

define( 'WPSHAPERE_VERSION' , '1.0.1' );

if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
    require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}


use Inc\Activate;
use Inc\Deactivate;
use Inc\PageTemplater;
use Inc\AcmeeFramework;
use Inc\Contact;


/*
*   WPSHAPERE Path Constant
*/
define( 'WPSHAPERE_PATH' , dirname(__FILE__) . "/");

/*
*   WPSHAPERE URI Constant
*/
define( 'WPSHAPERE_DIR_URI' , plugin_dir_url(__FILE__) );
if(is_multisite())
    define('NETWORK_ADMIN_CONTROL', true);
// Delete the above two lines to enable customization per blog
if ( !class_exists( 'Adminpanel_appsaz' ) ) {

    class Adminpanel_appsaz
    {
        public $plugin;

        public  $setting;

        function __construct()
        {
            $this->plugin = plugin_basename(__FILE__);



        }

        function register()
        {
            $login_change= get_option('wpappsaz_options')['admin_page_login'];
           // add_action('template_redirect',array($this,'appsaz_redirect_all_pages_to_home'));

            add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );

            if($login_change==1){
                add_action( 'wp_before_admin_bar_render', array($this,'appsaz_remove_old_logout') );
                add_action( 'admin_bar_menu', array($this,'appsaz_newlogout_box') );
                $loginpage =ABSPATH . 'wp-content/plugins/Appsaz-Admin-panel/themplate/appsaz-login.php';
                $loginbase =ABSPATH . 'wp-content/plugins/Appsaz-Admin-panel/themplate/wp-login.php';
                $loginpage1 = ABSPATH .'appsaz-login.php';
                $loginbase1 = ABSPATH .'wp-login.php';
                if(file_exists ($loginpage)) {
                    rename($loginpage, $loginpage1);
                    rename($loginbase1, $loginbase);

                }
            }
            else{

                $loginpage =ABSPATH . 'wp-content/plugins/Appsaz-Admin-panel/themplate/appsaz-login.php';
                $loginbase =ABSPATH . 'wp-content/plugins/Appsaz-Admin-panel/themplate/wp-login.php';

                $loginpage1 = ABSPATH .'appsaz-login.php';
                $loginbase1 = ABSPATH .'wp-login.php';
                if(file_exists ($loginpage1)) {
                    rename($loginpage1, $loginpage);
                    rename($loginbase, $loginbase1);
                }
            }
            add_filter( 'plugin_action_links_' . $this->plugin,  array( $this, 'aai_plugin_action_links' ) );

            add_action( 'init', array($this, 'load_plugin_textdomain' ) );

            if ( file_exists( dirname( __FILE__ ) . '/inc/Settingpage.php' ) ) {
                require_once dirname( __FILE__ ) . '/inc/Settingpage.php';
            }
            if(!is_multisite()) {
                $multi_option = false;
            }
            elseif(is_multisite() && !defined('NETWORK_ADMIN_CONTROL')) {
                $multi_option = false;
            }
            else {
                $multi_option = true;
            }
            $config = array(
                'capability' => 'manage_options',
                'page_title' => __('تنظیمات', 'appsaz-admin'),
                'menu_title' => __('Appsaz-Admin-Interface', 'appsaz-admin'),
                'menu_slug' => 'wpappsaz-options',
                'icon_url'   => 'dashicons-art',
                'position'   => 3,
                'tabs'  => $panel_tabs,
                'fields'    => $panel_fields,
                'multi' => $multi_option //default = false
            );
            $aof_options = new AcmeeFramework($config);

            if ( file_exists( dirname( __FILE__ ) . '/inc/Adminpanel_appsaz_class.php' ) ) {
                require_once dirname( __FILE__ ) . '/inc/Adminpanel_appsaz_class.php';
            }
        }
        function appsaz_remove_old_logout() {
            global $wp_admin_bar;
            $wp_admin_bar->remove_node('my-account');
        }
        function appsaz_newlogout_box() {
            ?>
            <table id="one-click01" style="" border="0" cellspacing="0" cellpadding="0"><tr><td align=center valign=center>
                        <?php
                        wp_get_current_user();
                        $current_user = wp_get_current_user();
                        if ( !($current_user instanceof WP_User) )
                            return;
                        $name = $current_user->display_name;
                        ?>

                        <?php echo '
                                        <a href="/appsaz-login.php?action=logout" title="' . esc_attr__('Log Out') . '">Howdy, ' . __($name) . '</a>'
                        ; ?>

                    </td></tr></table>

            <?php
        }
        function appsaz_redirect_all_pages_to_home() {
          /*  $array_page=array();
            $activePlugin=new Activate;
            $page_home_id= $activePlugin::create_home_page();*/
            $array_page=explode(',' , get_option('wpappsaz_options')['front_id_page']);
            $redirect_value=get_option('wpappsaz_options')['disable_redirect_login'];
            if($redirect_value==1) {
                        wp_redirect(get_permalink('/'));
            }
        }
        function enqueue()
        {
            wp_enqueue_style('main', plugins_url('/public/assets/css/main.css', __FILE__));
        }

        public  function aai_plugin_action_links($links) {
            $setting_links = '<a href="options-general.php?page=wpappsaz-options">'.__("setting","appsaz-admin").'</a>';
            array_push($links,$setting_links);
            return $links;
        }

        public function load_plugin_textdomain() {
            load_plugin_textdomain( 'appsaz-admin', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
        }
    }


    $firstpage=new Adminpanel_appsaz();
    $firstpage->register();

    $PageTemplater=new PageTemplater();
    $PageTemplater->register();

    $contact=new Contact();
    $contact->register();


    $activePlugin=new Activate;

    $dectivePlugin=new Deactivate;




    register_activation_hook( __FILE__, array( $activePlugin, 'activate' ) );

    // deactivation
    register_deactivation_hook( __FILE__, array( $dectivePlugin, 'deactivate' ) );
}

